﻿using System;

namespace SimCorpTask
{
    public class DepositCalculator
    {
        public event EventHandler<string> ValidError;

        private readonly IValidInput validInput;

        const int NumberOfMonthInYear = 12;


        public DepositCalculator(IValidInput validInput)
        {
            this.validInput = validInput;
        }

        public double CalculateSumOfPayment(int x,float R,float N,DateTime agreementDate,DateTime calculationDate)
        {
            var validResult = validInput.CheckInput(x, R, N, agreementDate, calculationDate);
            if (validResult.Item1)
            {               
                var paidPeriod = GetTotalMonthsFrom(agreementDate, calculationDate);

                var restPeriod = N * NumberOfMonthInYear - paidPeriod;
                
                var numberOfPayments = N * NumberOfMonthInYear;

                var interestRate = R / (NumberOfMonthInYear * 100);

                var monthlyPay = x * ((interestRate * Math.Pow((1 + interestRate), numberOfPayments)) 
                    /(Math.Pow((1 + interestRate), numberOfPayments) - 1));

                var sumPay = monthlyPay* restPeriod;

                return sumPay;
            }
            else
            {
                OnError(validResult.Item2);
                return 0;
            }
        }

        private static int GetTotalMonthsFrom(DateTime startDate, DateTime endDate)
        {
            int monthsDiff = 1;
            while (startDate.AddMonths(monthsDiff) <= endDate)
            {
                monthsDiff++;
            }
            return monthsDiff - 1;
        }

        protected virtual void OnError(string input)
        {
            ValidError?.Invoke(this, input);
        }
    }
}
