﻿using System;

namespace SimCorpTask
{
    public class ConsoleErrorPrinter : IErrorPrinter
    {
        public void OnError(object source, string args)
        {
            Console.WriteLine("\n{0}", args);
        }
    }
}
