﻿namespace SimCorpTask
{
    public interface IErrorPrinter
    {
        void OnError(object source, string args);
    }
}
