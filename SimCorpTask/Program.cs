﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text;

namespace SimCorpTask
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MainWork();
        }

        static void MainWork()
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            using (serviceProvider as IDisposable)
            {
                var depositcalculator = serviceProvider.GetService<DepositCalculator>();
                var parserGeneric = serviceProvider.GetService<ParserGeneric>();
                var consoleErrorPrinter = serviceProvider.GetService<IErrorPrinter>();

                depositcalculator.ValidError += consoleErrorPrinter.OnError;
                parserGeneric.ValidError += consoleErrorPrinter.OnError;

                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                var enc1251 = Encoding.GetEncoding(1251);

                Console.OutputEncoding = Encoding.UTF8;
                Console.InputEncoding = enc1251;

                while (true)
                {
                    
                    Console.WriteLine("Вас приветствует счетчик прибыли от вложения в бизнес,для подсчета прибыли введите Счет" +
                        ",для выхода - Выход.");

                    var input = Console.ReadLine();                 

                    if (input == "Счет")
                    {
                        Console.WriteLine("\nВведите сумму займа:\n");

                        if (parserGeneric.TryParse<int>(Console.ReadLine(), out int money))
                        {
                            Console.WriteLine("\nВведите процентную ставку в процентах:\n");

                            if (parserGeneric.TryParse(Console.ReadLine(), out float interestRate))
                            {
                                Console.WriteLine("\nВведите длитеность займа в годах:\n");

                                if (parserGeneric.TryParse(Console.ReadLine(), out float investmentDuration))
                                {
                                    Console.WriteLine("\nВведите дату заключения договора:\n");

                                    if (parserGeneric.TryParse(Console.ReadLine(), out DateTime agreementDate))
                                    {
                                        Console.WriteLine("\nВведите дату просчета:\n");

                                        if (parserGeneric.TryParse(Console.ReadLine(), out DateTime calculateDate))
                                        {
                                            var income = depositcalculator.CalculateSumOfPayment(money, interestRate,
                                                investmentDuration, agreementDate, calculateDate);
                                            Console.WriteLine("\nЗа указанный период времени вы заработаете {0:0.##}.\n", income);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (input == "Выход")
                    {
                        break;
                    }
                    else { Console.WriteLine("\nКоманда не распознана, попробуйте снова.\n"); }
                }
            }

        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<DepositCalculator>();
            serviceCollection.AddSingleton<ParserGeneric>();
            serviceCollection.AddSingleton<IValidInput, ValidInput>();
            serviceCollection.AddSingleton<IErrorPrinter, ConsoleErrorPrinter>();
        }
    }
}
