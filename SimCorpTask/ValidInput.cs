﻿using System;

namespace SimCorpTask
{
    public class ValidInput : IValidInput
    {
        private static string[] errorList = new[] {"Ошибка,сумму займа должна быть больше 0!",
        "Ошибка,процентная ставка должна быть больше 0!",
        "Ошибка,длитеность займа должна быть 3 месяца или больше",
        "Ошибка,дата калькуляции не может быть меньше даты соглашения!",
        "Ошибка,дата калькуляции не может быть больше даты последнего платежа!"};

        const int NumberOfMonthInYear = 12;

        public Tuple<bool, string> CheckInput(int x, float R, float N, DateTime agreementDate, DateTime calculationDate)
        {
            var period = GetTotalMonthsFrom(agreementDate, calculationDate);

            if (x <= 0)
                return Tuple.Create(false, errorList[0]);
            else if (R <= 0)
                return Tuple.Create(false, errorList[1]);
            else if (N < 0.25)
                return Tuple.Create(false, errorList[2]);
            else if (calculationDate<agreementDate)
                return Tuple.Create(false, errorList[3]);
            else if (period > N*NumberOfMonthInYear)   
                return Tuple.Create(false, errorList[4]);
            return Tuple.Create(true, "Ошибки отсутствуют");
        }

        private static int GetTotalMonthsFrom(DateTime startDate, DateTime endDate)
        {
            int monthsDiff = 1;
            while (startDate.AddMonths(monthsDiff) <= endDate)
            {
                monthsDiff++;
            }
            return monthsDiff - 1;
        }
    }
}
