﻿using System;

namespace SimCorpTask
{
    public interface IValidInput
    {
        Tuple<bool, string> CheckInput(int x, float R, float N, DateTime agreementDate, DateTime calculationDate);
    }
}
