﻿using System;
using System.Globalization;

namespace SimCorpTask
{
    public class ParserGeneric
    {
        private static readonly NumberFormatInfo currentformat = NumberFormatInfo.CurrentInfo;

        private static readonly string separator = currentformat.CurrencyDecimalSeparator;

        private static string[] errorList = new[] {"Ошибка,число должно быть целым.Попробуйте снова.",
        "Ошибка,число должно быть дробным с разделителем -" + separator + ".Попробуйте снова .",
        "Ошибка,запишите дату верно например - 22.03.2022.Попробуйте снова."};

        public event EventHandler<string> ValidError;

        public bool TryParse<T>(string text, out T value)
        {
            value = default(T);
            try
            {
                value = (T)Convert.ChangeType(text, typeof(T));

                return true;
            }
            catch
            {
                switch (typeof(T).Name)
                {
                    case "Int32":
                        OnError(errorList[0]);
                        break;
                    case "Single":
                        OnError(errorList[1]);
                        break;
                    case "DateTime":
                        OnError(errorList[2]);
                        break;
                }             
                return false;
            }
        }

        protected virtual void OnError(string input)
        {
            ValidError?.Invoke(this, input);
        }
    }
}
