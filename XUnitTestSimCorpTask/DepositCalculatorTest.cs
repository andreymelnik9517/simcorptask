using System;
using Xunit;
using SimCorpTask;
using Moq;

namespace XUnitTestSimCorpTask
{
    public class DepositCalculatorTest
    {
        [Fact]
        public void CalculateSumOfPaymentInValidInputTest()
        {
            //arrange

            var x = 1200;
            var R = 10;
            var N = 2;
            var agreementDate = DateTime.Parse("26.05.2022");
            var calculationDate = DateTime.Parse("12.03.2021");

            var expectedResultValidInput = Tuple.Create(false, "������ ���������");

            var mockValidInput = new Mock<IValidInput>();

            mockValidInput.Setup(valid => valid.CheckInput(x, R, N, agreementDate, calculationDate)).Returns(expectedResultValidInput);

            var depositcalculator = new DepositCalculator(mockValidInput.Object);

            var expectedResultDepositcalculator = 0;

            //act

            var actualResult = (int)depositcalculator.CalculateSumOfPayment(x, R, N, agreementDate, calculationDate);

            //assert

            Assert.Equal(expectedResultDepositcalculator, actualResult);

        }

        [Fact]
        public void CalculateSumOfPaymentValidInputTest()
        {
            //arrange
            
            var x = 1200;
            var R = 10;
            var N = 2;
            var agreementDate = DateTime.Parse("26.05.2022");
            var calculationDate = DateTime.Parse("12.03.2023");

            var expectedResultValidInput = Tuple.Create(true, "������ �����������");

            var mockValidInput = new Mock<IValidInput>();

            mockValidInput.Setup(valid => valid.CheckInput(x,R,N,agreementDate,calculationDate)).Returns(expectedResultValidInput);

            var depositcalculator = new DepositCalculator(mockValidInput.Object);

            var expectedResultDepositcalculator = 830;

            //act

            var actualResult = (int)depositcalculator.CalculateSumOfPayment(x, R, N, agreementDate, calculationDate);

            //assert

            Assert.Equal(expectedResultDepositcalculator,actualResult);

        }
    }
}
